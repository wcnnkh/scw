package scw.context;

import scw.instance.ServiceLoaderFactory;

public interface ProviderLoaderFactory extends ServiceLoaderFactory, ProviderClassesLoaderFactory{
}
